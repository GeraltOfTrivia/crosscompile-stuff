# Using QEMU

There's also the choice of cross-compiling via QEMU.

## Setup
These instructions will describe how to set up an Ubuntu 18.04 aarch64 environment using ``debootstrap`` and ``qemu-user-static``

* You need to have the following packages installed on your computer before you proceed further: ``debootstrap qemu-user-static``

        $ sudo apt install debootstrap qemu-user-static

* Create an arm64 base system in folder ``arm64-bionic``


    ``$ sudo qemu-debootstrap --arch=arm64 bionic arm64-bionic``

* Mount ``proc dev sys`` 
   
        $ sudo mount -o bind /dev arm64-bionic/dev
        $ sudo mount -o bind /proc arm64-bionic/proc
        $ sudo mount -o bind /sys arm64-bionic/sys
      
* Chroot into the arm64 environment and verify the architecture

        $ sudo chroot arm64-bionic
        # uname -m

You should get ``aarch64`` as a result.

* Edit ``/etc/apt/sources.list``

Add ``universe multiverse`` after ``main``
Copy the first line into a new line and change ``bionic`` to ``bionic-updates``. Save and exit.
* Run ``apt update`` and ``apt upgrade``

## Build

Install the dependencies and start the build:

    # apt install build-essential cmake pkg-config libboost-all-dev libssl-dev libzmq3-dev libunbound-dev libsodium-dev libunwind8-dev liblzma-dev libreadline6-dev libldns-dev libexpat1-dev doxygen graphviz libpgm-dev nasm
    # cd ryo-currency
    # make -j x

*x* is the number of processor cores you want to use 

If there are no errors, the binaries should be located at : ``ryo-currency/build/release/bin`` within the chroot image.

## Cleanup
After you finish and exit the chroot environment (with ``exit`` obviously) it's recommended to unmount everything again:

    $ sudo umount arm64-bionic/dev
    $ sudo umount arm64-bionic/proc
    $ sudo umount arm64-bionic/sys
    
Done.
Of course this can be used not only to (cross)compile the binaries mentioned here but  *almost* anything that can be compiled on armv6/armv7.

**Note:** The method described here produces binaries compatible with SBCs, **NOT** with Android.

### Pros and Cons
**Pros**

 - Builds complete much quicker than on the actual hardware
 - No need to build dependencies from source
 - Versatile, can be used for other applications too
 - Built binaries can be actually executed in the QEMU environment, good for some very basic testing (see if the binaries actually run).
 
 **Cons**
 
 - Not as straightforward as a do-it-all bash script or dockerfile
 - Slower than a bash script
 